﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPIDemo.Models;

namespace WebAPIDemo.Controllers
{
    public class EmployeeController : ApiController
    {
        ////Get Employee Lists
        //public IEnumerable<EmployeeDetail> Get()
        //{
        //    using (EmployeDBEntities entities = new EmployeDBEntities())
        //    {
        //       return entities.EmployeeDetails.ToList();
        //    }
        //}

        ////Get specific employee details
        //public HttpResponseMessage Get(int id)
        //{
        //    using (EmployeDBEntities entities = new EmployeDBEntities())
        //    {
        //         var entity= entities.EmployeeDetails.FirstOrDefault(e => e.ID == id);
        //        if(entity ==null)
        //        {
        //            return Request.CreateResponse(HttpStatusCode.NotFound, "Employee Id = " + id.ToString() + " not found");
        //        }
        //        else
        //        {
        //            return Request.CreateResponse(HttpStatusCode.OK, entity);
        //        }
        //    }
        //}

        ////Get add employee details
        //public HttpResponseMessage Post([FromBody]EmployeeDetail emp) 
        //{
        //    try
        //    {
        //        using (EmployeDBEntities entities = new EmployeDBEntities())
        //        {
        //            entities.EmployeeDetails.Add(emp);
        //            entities.SaveChanges();
        //            var message= Request.CreateResponse(HttpStatusCode.Created, emp);
        //            message.Headers.Location = new Uri(Request.RequestUri + emp.ID.ToString());
        //            return message;
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
        //    }
        //}

        ////Get delete specific employee details
        //public HttpResponseMessage Delete(int id)
        //{

        //        using (EmployeDBEntities entities = new EmployeDBEntities())
        //        {
        //        var entity = entities.EmployeeDetails.FirstOrDefault(e => e.ID == id);
        //        if(entity ==null)
        //        {
        //            return Request.CreateResponse(HttpStatusCode.BadRequest, "Employee ID = " + id.ToString() + " not found to be delete");
        //        }
        //        else
        //        {
        //            entities.EmployeeDetails.Remove(entity);
        //            entities.SaveChanges();
        //            return Request.CreateResponse(HttpStatusCode.OK);          
        //        }
        //    }           
        //}

        ////Get add employee details
        //public HttpResponseMessage Put(int id, [FromBody]EmployeeDetail emp)
        //{
        //    using (EmployeDBEntities entities = new EmployeDBEntities())
        //    {
        //        var entity = entities.EmployeeDetails.FirstOrDefault(e => e.ID == id);
        //        if (entity == null)
        //        {
        //            return Request.CreateResponse(HttpStatusCode.BadRequest, "Employee ID = " + id.ToString() + " not found to be update");
        //        }
        //        else
        //        {
        //            entity.FirstName = emp.FirstName;
        //            entity.LastName = emp.LastName;
        //            entity.Gender = emp.Gender;
        //            entity.Address = emp.Address;
        //            entities.SaveChanges();
        //            return Request.CreateResponse(HttpStatusCode.OK);
        //        }
        //    }
        //}



       [HttpGet]
        public IEnumerable<EmployeeDetail> LoadEmployees()
        {
            using (EmployeDBEntities entities = new EmployeDBEntities())
            {
                return entities.EmployeeDetails.ToList();
            }
        }

        //GetEmployeeById
        [HttpGet]
        public HttpResponseMessage EmployeeById(int id)
        {
            using (EmployeDBEntities entities = new EmployeDBEntities())
            {
                var entity = entities.EmployeeDetails.FirstOrDefault(e => e.ID == id);
                if (entity == null)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Employee Id = " + id.ToString() + " not found");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
            }
        }

        //PostEmployeeDetails
        //InsertEmployeeDetails
        [HttpPost]
        public HttpResponseMessage AddEmployeeDetails([FromBody]EmployeeDetail emp)
        {
            try
            {
                using (EmployeDBEntities entities = new EmployeDBEntities())
                {
                    entities.EmployeeDetails.Add(emp);
                    entities.SaveChanges();
                    var message = Request.CreateResponse(HttpStatusCode.Created, emp);
                    message.Headers.Location = new Uri(Request.RequestUri + emp.ID.ToString());
                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        //DeleteEmployee
        [HttpDelete]
        public HttpResponseMessage SpecificEmployeeDelete(int id)
        {

            using (EmployeDBEntities entities = new EmployeDBEntities())
            {
                var entity = entities.EmployeeDetails.FirstOrDefault(e => e.ID == id);
                if (entity == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Employee ID = " + id.ToString() + " not found to be delete");
                }
                else
                {
                    entities.EmployeeDetails.Remove(entity);
                    entities.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
        }
    }
}
